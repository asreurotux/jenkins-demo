#!/bin/bash
export HTTPS_PROXY=http://10.10.4.254:3128/
yum -y install python-virtualenv
yum -y install gettext-devel
cd /webapp/
virtualenv env
source env/bin/activate                 # Activate the virtualenv
cd djangoapp/
pip install --upgrade setuptools pip # Update setuptools
pip install -r djangoapp/requirements/dev.txt           # Install or upgrade dependencies
python manage.py migrate                  # Apply South's database migrations
#python manage.py compilemessages          # Create translation files
python manage.py collectstatic --noinput  # Collect static files
python -W ignore manage.py test --noinput --settings=djangoapp.settings.dev # Run the tests
