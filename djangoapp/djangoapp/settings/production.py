from .base import *


SECRET_KEY = os.environ['SECRET_KEY']

DEBUG = False

COMPRESS_OFFLINE = True

MIDDLEWARE_CLASSES += (
    'django.middleware.gzip.GZipMiddleware',
)
